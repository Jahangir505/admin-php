<?php
	require_once('functions/function.php');
	get_header();
	get_sidebar();
	get_bread();

	$id=$_GET['u'];
	$sel="SELECT * FROM cit_users WHERE user_id='$id'";
	$Q=mysqli_query($con,$sel);
	$data=mysqli_fetch_assoc($Q);

	if(!empty($_POST)){
		$name=$_POST['name'];
		$phone=$_POST['phone'];
		$email=$_POST['email'];
		$role=$_POST['role'];
		if(!empty($name)){
			if(!empty($email)){
				$edit="UPDATE cit_users SET user_name='$name',user_phone='$phone',user_email='$email', role_id='$role' WHERE user_id='$id'";
				if(mysqli_query($con,$edit)){
					header('Location: view-user.php?v='.$id);
				}else{
					echo "Update Failed! Please Try Again.";
				}
			}else{
				echo "Please enter your email address!";
			}
		}else{
			echo "Please enter your name!";
		}
	}
?>
    <div class="col-md-12">
        <form class="form-horizontal" action="" method="post">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="col-md-9 heading_title">
                    Update User Information
                 </div>
                 <div class="col-md-3 text-right">
                    <a href="all-user.php" class="btn btn-sm btn btn-primary"><i class="fa fa-th"></i> All User</a>
                </div>
                <div class="clearfix"></div>
            </div>
          <div class="panel-body">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="name" value="<?= $data['user_name'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="phone" value="<?= $data['user_phone'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" name="email" value="<?= $data['user_email'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Username</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="user" value="<?= $data['user_username'];?>" disabled="disabled">
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">User Role</label>
                <div class="col-sm-4">
                  <select class="form-control select_cus" name="role">
                      <option>Select Role</option>
											<?php
													$sel="SELECT * FROM cit_roles ORDER BY role_id ASC";
													$Q=mysqli_query($con,$sel);
													while($role=mysqli_fetch_assoc($Q)){
											?>
                      <option value="<?= $role['role_id'];?>" <?php if($data['role_id']==$role['role_id']){echo 'selected="selected"';}?>><?= $role['role_name'];?></option>
										<?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Photo</label>
                <div class="col-sm-8">
                  <input type="file" name="pic">
                </div>
              </div>
          </div>
          <div class="panel-footer text-center">
            <button class="btn btn-sm btn-primary">UPDATE</button>
          </div>
        </div>
        </form>
    </div><!--col-md-12 end-->
<?php
	get_footer();
?>
